set number relativenumber

set columns=128
set lines=40
set colorcolumn=80,120
set textwidth=80
set ruler

set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab

syntax on

colorscheme desert

set autoindent
set smartindent
set backspace=indent,eol,start
set hlsearch
set hidden
set formatoptions=cqtn

augroup project
    autocmd!
    autocmd BufRead,BufNewFile *.h,*.c set filetype=c.doxygen
augroup END

"
" vimwiki
"
set nocompatible
filetype plugin on
syntax on
let g:vimwiki_ext2syntax = {'.md': 'markdown', '.markdown': 'markdown', '.mkd': 'markdown', '.mdown': 'markdown'}

" 
" vim-instant-markdown
"
let g:instant_markdown_autostart = 0
map <leader>md :InstantMarkdownPreview<CR>

" Edit your .vimrc
"
call plug#begin('~/.vim/plugged')
"
    Plug 'WolfgangMehner/c-support'
    Plug 'universal-ctags/ctags'

    Plug 'junegunn/vim-easy-align'

    Plug 'scrooloose/nerdtree'

    Plug 'vimwiki/vimwiki'
    Plug 'suan/vim-instant-markdown'
"   " Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
"   " Any valid git URL is allowed
"   " Make sure you use single quotes
"   Plug 'https://github.com/junegunn/vim-github-dashboard.git'
"
"   " Multiple Plug commands can be written in a single line using | separators
"   Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
"
"   " On-demand loading
"   Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
"   Plug 'tpope/vim-fireplace', { 'for': 'clojure' }
"
"   " Using a non-master branch
"   Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }
"
"   " Using a tagged release; wildcard allowed (requires git 1.9.2 or above)
"   Plug 'fatih/vim-go', { 'tag': '*' }
"
"   " Plugin options
"   Plug 'nsf/gocode', { 'tag': 'v.20150303', 'rtp': 'vim' }
"
"   " Plugin outside ~/.vim/plugged with post-update hook
"   Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
"
"   " Unmanaged plugin (manually installed and updated)
"   Plug '~/my-prototype-plugin'
"
"   " Initialize plugin system
   call plug#end()
